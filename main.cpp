//
// main.cpp
// Reads raw data from DT5560SE file into ROOT format
// Created by Nathan Giha (giha@umich.edu) on Jan. 30, 2023
// SLAC National Accelerator Laboratory / U. Michigan
//

#include <stdlib.h>
#include <stdio.h>
#include <TString.h>
#include <TFile.h>

#include "fileHandling.h"
#include "UserInput.h"
#include "EventClass.h"
#include "DataTree.h"
#include "LZTree.h"

using namespace std;

int main(int argc, char** argv)
{

  cout << "\n Welcome to READ_DATA" << endl;

  TString fileName;
  TString inputFileName;

  if(argc == 1) {
    cout << "ERROR: input file not given\n";
    return 0;
  }


  // Handle input file from cmd line arg

  inputFileName = TString(argv[1]);


  cout << " Reading input from " << inputFileName << endl;
  UserInput* input = new UserInput();
  input->readInput(inputFileName);

  cout << " Max # of events set to " << input->MAX_EVENTS << endl;


  cout << " Initializing event structure based on user input" << endl;
  cout << " h: " << input->HEADER_LENGTH
       << " ch: " << input->NUM_CHANNELS << " sa: " << input->RECORD_LENGTH
       << endl;
  EventClass* event = new EventClass(input);

  cout << " Creating ROOT output: " << input->OUTPUT_FILE_NAME << endl;
  DataTree* outData = new DataTree(input, event);

  // Fill Root tree from raw data files
  for (Int_t file = 0; file < input->NUM_FILES; file++) // CURRENTLY only handles one file
  {
    bool openedRaw = true;

    TString thisFileName = input->RAW_FILE_NAME;

    cout << " Opening raw data file: " << thisFileName << endl;
    ifstream rawDataStream;
    openedRaw = openFile(rawDataStream, thisFileName);

    if(openedRaw)
    {

      Int_t numEvents = 0;
      bool doLoop = true;

      while(numEvents < input->MAX_EVENTS
           && event->readEventFromFile(rawDataStream, ""))
      {
        // cout << " Filling\n";
        outData->fillTrees(event);
        // outData->headerTree->Fill();
        // outData->
        event->resetEvent();
        numEvents++;
      }

      outData->writeToFile();

      cout << " Closing raw file\n";
      rawDataStream.close();
    }
    else
    {
      cout << "ERROR: could not open file " << thisFileName << endl;
    }
  }

  if (input->GEN_LZ_FILE)
  {
    // Fill LZ data tree
    cout << "Creating LZ data file: " << input->LZ_FILE_NAME << endl;

    LZTree* outLZ = new LZTree(input);
    outLZ->FillLZTree(input, outData);
    // outLZ->closeFiles();
  }
  cout << "\n";
  outData->closeFile();

  return 0;
}
