//
//  fileHandling.h
//  Function prototype for opening a file and performing error-checking
//
//  Created by Nathan Giha on 11/6/20.
//

#ifndef _FILE_HANDLING_H_
#define _FILE_HANDLING_H_

#include <TString.h>

#include <string>
#include <fstream>

using namespace std;

// Opens file and performs error checking, so that the file stream
// can be passed other functionality
// Returns true if successful
bool openFile(ifstream &inFile,
              const TString inFileName);

// Checks file stream for errors
bool checkFileRead(ifstream &inFile,
                   const TString errorMessage);

#endif /* openFile_h */
