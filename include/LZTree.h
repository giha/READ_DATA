//
// LZTree.h
// Header for an output root tree
// Class for exporting "LZ-format" ROOT tree from pre-processed ROOT data
// Created by Nathan Giha (giha@umich.edu) on Mar. 13, 2023
// SLAC National Accelerator Laboratory / U. Michigan
//

#ifndef _LZTREE_H_
#define _LZTREE_H_

#include <TROOT.h>
#include <TTree.h>
// #include <TChain.h>
#include <TFile.h>
#include <TString.h>

#include "UserInput.h"
#include "DataTree.h"
#include "readHeaders.h"
#include "readWfs.h"
//
#include <string>
#include <iostream>
#include <fstream>

using namespace std;

class LZTree
{
public:
  DataTree*  inData;

  readHeaders* headers;
  readWfs*     wfs;

  // Output file and trees
  TFile*     lzFile;
  TTree*     globalTree;
  TTree*     summaryTree;
  TTree*     eventTree;
  TTree*     dataTree;
  TTree*     sumDataTree;

  // Output vars
  // Global TTree
  UInt_t    triggerRunNumber;
  UShort_t  runType;
  UShort_t  firstEv;
  UChar_t   numberOfDigitizers[14];
  UShort_t  numberOfChannels[14];
  std::vector <short int> chDC0vec = std::vector <short int>(32);

  // Event TTree
  UInt_t    eventSeqNumber;
  UInt_t    firstEvtData;
  UShort_t  numOfPODs[15];
  ULong64_t bufferLiveStartTS[46];
  ULong64_t bufferLiveStopTS[46];
  UShort_t  triggerType_Short;
  ULong64_t triggerTimeStamp;
  UShort_t  triggerMultiplicity;

  // Data TTree
  UShort_t  nEvtsFile;
  UShort_t  channelID;
  UShort_t  hitId;
  ULong64_t startTS;
  UShort_t  lengthPOD;
  UChar_t   truncPOD;
  std::vector <short int> myPODvec = std::vector <short int>(2000);

  // SumData TTree
  UShort_t  TRevt;
  ULong64_t TRstartTS;
  UShort_t  TRlengthPOD;
  UChar_t   TRtruncPOD;
  std::vector <short int> sumPODvec = std::vector <short int>(2000);

  // Summary TTree
  UShort_t  endFlag;

  LZTree(UserInput* inUserInput);

  void FillLZTree(UserInput* inUserInput, DataTree* inData);
  void InitGlobal();
  void InitSummary();
  void InitEvent();
  void InitData();
  void InitSumData();

  // void writeToTree();
  void closeFiles();
};



#endif /* DataTree_h */
