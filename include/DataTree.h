//
// DataTree.h
// Header for an output root tree
// Class for exporting Root Tree from DT5560SE
// Created by Nathan Giha (giha@umich.edu) on Jan. 30, 2023
// SLAC National Accelerator Laboratory / U. Michigan
//

#ifndef _DATATREE_H_
#define _DATATREE_H_

#include <TROOT.h>
#include <TTree.h>
// #include <TChain.h>
#include <TFile.h>
#include <TString.h>

#include "UserInput.h"
#include "EventClass.h"
//
#include <string>
#include <iostream>
#include <fstream>
// #include <sstream>
// #include <cstring>

using namespace std;

class DataTree
{
private:

public:
  UserInput* input;
  TFile* outFile;
  TTree* headerTree;
  TTree* wfTree;

  UShort_t chan;
  UShort_t* wf;

  DataTree(UserInput* inUserInput, EventClass* event);

  void fillTrees(EventClass* event);
  void writeToFile();
  void closeFile();

};



#endif /* DataTree_h */
