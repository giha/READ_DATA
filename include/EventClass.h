//
// EventClass.h
// Header for a digitizer event
// Class for reading raw data from DT5560SE
// Created by Nathan Giha (giha@umich.edu) on Jan. 30, 2023
// SLAC National Accelerator Laboratory / U. Michigan
//
#ifndef RD_EVENTCLASS_H_
#define RD_EVENTCLASS_H_

#include <string>
#include <iostream>
#include <fstream>

#include "fileHandling.h"
#include "UserInput.h"

using namespace std;

class EventClass
{
public:
  UInt_t*       header = NULL;
  UShort_t**    waveforms = NULL;
  UserInput*    userInp = NULL;
  UInt_t        evtNum = 0;

  // Default constructor initializes params to zero
  EventClass(UserInput* inUserInput);

  void setUserInput(UserInput* inUserInput);

  bool readEventFromFile(ifstream &inFile,
                         const string errorMessage);


  // Resets header/wf to be zero-valued
  void resetEvent();

  // Deletes dynamically allocated memory for event
  void deleteEvent();
};



























#endif /* EventClass_h */
