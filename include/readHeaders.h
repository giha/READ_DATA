//////////////////////////////////////////////////////////
// This class has been automatically generated on
// Tue Apr  4 15:45:25 2023 by ROOT version 6.24/06
// from TTree headers/headers
// found on file: output.root
//////////////////////////////////////////////////////////

#ifndef readHeaders_h
#define readHeaders_h

#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>

// Header file for the classes stored in the TTree if any.

class readHeaders {
public :
   TTree          *fChain;   //!pointer to the analyzed TTree or TChain
   Int_t           fCurrent; //!current Tree number in a TChain

// Fixed size dimensions of array or collections stored in the TTree if any.

   // Declaration of leaf types
   UInt_t          packetCounter;
   UInt_t          trigCounts;
   UInt_t          sumOut;
   UInt_t          timeCoarse;
   UInt_t          timeFine;

   // List of branches
   TBranch        *b_packetCounter;   //!
   TBranch        *b_trigCounts;   //!
   TBranch        *b_sumOut;   //!
   TBranch        *b_timeCoarse;   //!
   TBranch        *b_timeFine;   //!

   readHeaders(TTree *tree=0);
   virtual ~readHeaders();
   virtual Int_t    Cut(Long64_t entry);
   virtual Int_t    GetEntry(Long64_t entry);
   virtual Long64_t LoadTree(Long64_t entry);
   virtual void     Init(TTree *tree);
   virtual void     Loop();
   virtual Bool_t   Notify();
   virtual void     Show(Long64_t entry = -1);
};

#endif

#ifdef readHeaders_cxx
readHeaders::readHeaders(TTree *tree) : fChain(0) 
{
// if parameter tree is not specified (or zero), connect the file
// used to generate this class and read the Tree.
   if (tree == 0) {
      TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("output.root");
      if (!f || !f->IsOpen()) {
         f = new TFile("output.root");
      }
      f->GetObject("headers",tree);

   }
   Init(tree);
}

readHeaders::~readHeaders()
{
   if (!fChain) return;
   delete fChain->GetCurrentFile();
}

Int_t readHeaders::GetEntry(Long64_t entry)
{
// Read contents of entry.
   if (!fChain) return 0;
   return fChain->GetEntry(entry);
}
Long64_t readHeaders::LoadTree(Long64_t entry)
{
// Set the environment to read one entry
   if (!fChain) return -5;
   Long64_t centry = fChain->LoadTree(entry);
   if (centry < 0) return centry;
   if (fChain->GetTreeNumber() != fCurrent) {
      fCurrent = fChain->GetTreeNumber();
      Notify();
   }
   return centry;
}

void readHeaders::Init(TTree *tree)
{
   // The Init() function is called when the selector needs to initialize
   // a new tree or chain. Typically here the branch addresses and branch
   // pointers of the tree will be set.
   // It is normally not necessary to make changes to the generated
   // code, but the routine can be extended by the user if needed.
   // Init() will be called many times when running on PROOF
   // (once per file to be processed).

   // Set branch addresses and branch pointers
   if (!tree) return;
   fChain = tree;
   fCurrent = -1;
   fChain->SetMakeClass(1);

   fChain->SetBranchAddress("packetCounter", &packetCounter, &b_packetCounter);
   fChain->SetBranchAddress("trigCounts", &trigCounts, &b_trigCounts);
   fChain->SetBranchAddress("sumOut", &sumOut, &b_sumOut);
   fChain->SetBranchAddress("timeCoarse", &timeCoarse, &b_timeCoarse);
   fChain->SetBranchAddress("timeFine", &timeFine, &b_timeFine);
   Notify();
}

Bool_t readHeaders::Notify()
{
   // The Notify() function is called when a new file is opened. This
   // can be either for a new TTree in a TChain or when when a new TTree
   // is started when using PROOF. It is normally not necessary to make changes
   // to the generated code, but the routine can be extended by the
   // user if needed. The return value is currently not used.

   return kTRUE;
}

void readHeaders::Show(Long64_t entry)
{
// Print contents of entry.
// If entry is not specified, print current entry
   if (!fChain) return;
   fChain->Show(entry);
}
Int_t readHeaders::Cut(Long64_t entry)
{
// This function may be called from Loop.
// returns  1 if entry is accepted.
// returns -1 otherwise.
   return 1;
}
#endif // #ifdef readHeaders_cxx
