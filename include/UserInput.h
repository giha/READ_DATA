//
// UserInput.h
// Reads input file
// Class for reading user inputs for reading raw data from DT5560SE
// Created by Nathan Giha (giha@umich.edu) on Jan. 30, 2023
// SLAC National Accelerator Laboratory / U. Michigan
//

#ifndef _USER_INPUT_H_
#define _USER_INPUT_H_

#include <TROOT.h>
#include <TFile.h>
#include <TString.h>
#include <TGraph.h>

#include <string.h>
#include <iostream>
#include <fstream>
#include <sstream>
#include <cstring>

#include "fileHandling.h"

using namespace std;

class UserInput
{
private:

public:
  TString   RAW_FILE_NAME;
  TString   OUTPUT_FILE_NAME;
  TString   LZ_FILE_NAME;
  UInt_t    GEN_LZ_FILE;
  UInt_t    NUM_FILES;
  Int_t*    FILE_LIST;
  UInt_t    MAX_EVENTS;

  UInt_t    NUM_CHANNELS;
  UInt_t    HEADER_LENGTH;
  TString*  HEADER_STRUCTURE;

  UInt_t    RECORD_LENGTH;

  UserInput()
  {
    NUM_FILES = 1;
    FILE_LIST = NULL;
    RAW_FILE_NAME = "test.dat";
    OUTPUT_FILE_NAME = "out.root";
    GEN_LZ_FILE = 0;
    LZ_FILE_NAME = "lzFile.root";
    MAX_EVENTS = (Int_t)1e8;

    NUM_CHANNELS = 32;
    HEADER_LENGTH = 16;
    HEADER_STRUCTURE = NULL;
    RECORD_LENGTH = 4096;
  }

  void readInput(TString inputFile)
  {
    ifstream file;

    if(!openFile(file, inputFile))
    {
      cout << "Failed to open input file\n";
      exit(0);
    }

    string line;
    string tag, value;
    while(file >> tag)
    {
      if(tag == "<RAW_FILE_NAME>:")
      {
        file >> value;
        RAW_FILE_NAME = value;
      }

      else if(tag == "<OUTPUT_FILE_NAME>:")
      {
        file >> value;
        OUTPUT_FILE_NAME = value;
      }

      else if(tag == "<GEN_LZ_FILE>:")
      {
        file >> value;
        GEN_LZ_FILE = stoi(value);
      }

      else if(tag == "<LZ_FILE_NAME>:")
      {
        file >> value;
        LZ_FILE_NAME = value;
      }

      else if(tag == "<NUM_FILES>:")
      {
        file >> value;
        NUM_FILES = stoi(value);
        FILE_LIST = new int[NUM_FILES];
        for(Int_t i=0; i<NUM_FILES; i++)
        {
          FILE_LIST[i] = i;
        }
      }

      else if(tag == "<FILE_LIST>:")
      {
        for(Int_t i=0; i<NUM_FILES; i++)
        {
          file >> value;
          FILE_LIST[i] = stoi(value);
        }
      }

      else if(tag == "<MAX_EVENTS>:")
      {
        file >> value;
        MAX_EVENTS = stoi(value);
      }

      else if(tag == "<NUM_CHANNELS>:")
      {
        file >> value;
        NUM_CHANNELS = stoi(value);
      }

      else if(tag == "<HEADER_LENGTH>:")
      {
        file >> value;
        HEADER_LENGTH = stoi(value);
      }

      else if(tag == "<HEADER_STRUCTURE>:")
      {
        HEADER_STRUCTURE = new TString[HEADER_LENGTH];
        for(Int_t i=0; i<HEADER_LENGTH; i++)
        {
          file >> value;
          if (value == "empty")
          {
            HEADER_STRUCTURE[i] = "";
          }
          else
          {
            HEADER_STRUCTURE[i] = value;
          }
        }
      }

      else if(tag == "<RECORD_LENGTH>:")
      {
        file >> value;
        RECORD_LENGTH = stoi(value);
      }


    }
    file.close();
  }

  ~UserInput()
  {
    delete FILE_LIST;
    delete HEADER_STRUCTURE;
  }
};

#endif
