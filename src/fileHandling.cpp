//
//  fileHandling.cpp
//  Function implementation for opening a file and performing error-checking
//
//  Created by Nathan Giha on 11/6/20.
//


#include <fstream>
#include <iostream>
#include <string>

#include "fileHandling.h"

using namespace std;

// openFile function
bool openFile(ifstream &inFile,
              const TString inFileName)
{
  bool validInput = true;

  string inputFileString = string(inFileName);

  inFile.open(inputFileString.c_str());

  if (inFile.fail())
  {
    cout << "Unable to open file." << endl;
    validInput = false;
  }
  return validInput;
}

// Checks file stream for errors
bool checkFileRead(ifstream &inFile,
                   const TString errorMessage)
{
  bool success = true;

  if (inFile.eof())
  {
    if (errorMessage != "")
    {
      cout << "ERROR: EOF before reading " << errorMessage << endl;
    }
    success = false;
  }

  if (success)
  {
    if (inFile.fail())
    {
      if (errorMessage != "")
      {
        cout << "ERROR: Could not read " << errorMessage << endl;
      }
      success = false;
    }
  }

  return success;

}
