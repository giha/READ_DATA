//
// EventClass.cpp
// Member functions for a digitizer event
// Class for reading raw data from DT5560SE
// Created by Nathan Giha (giha@umich.edu) on Jan. 30, 2023
// SLAC National Accelerator Laboratory / U. Michigan
//


#include "EventClass.h"

using namespace std;

// Member functions

bool EventClass::readEventFromFile(ifstream &inFile,
                                   const string errorMessage)
{
  bool success = true;


  success = checkFileRead(inFile, errorMessage);

  if(success)
  {
    // Read event header
    for(Int_t h=0; h<userInp->HEADER_LENGTH; h++)
    {
      inFile >> std::hex >> header[h];
    }

    // Read event waveforms
    UInt_t doubleSample = 0;
    for(Int_t i = 0; i < userInp->RECORD_LENGTH; i++)
    {
      for(Int_t ch = 0; ch < userInp->NUM_CHANNELS; ch+=2)
      {
        inFile >> std::hex >> doubleSample;

        waveforms[ch][i]   = UShort_t(doubleSample >> 16);
        waveforms[ch+1][i] = UShort_t(doubleSample & 0xffff);
      }
    }
  }

  // Handle error messages, if applicable
  success = checkFileRead(inFile, errorMessage);

  return success;
}


void EventClass::setUserInput(UserInput* inUserInput)
{
  userInp = inUserInput;
}


// Constructors
EventClass::EventClass(UserInput* inUserInput)
{
  userInp = inUserInput;

  header = new UInt_t[userInp->HEADER_LENGTH];
  for(Int_t i=0; i<userInp->HEADER_LENGTH; i++)
  {
    header[i] = 0;
  }

  waveforms = new UShort_t*[userInp->NUM_CHANNELS];
  for(Int_t ch=0; ch<userInp->NUM_CHANNELS; ch++)
  {
    waveforms[ch] = new UShort_t[userInp->RECORD_LENGTH];

    for(Int_t i=0; i<userInp->RECORD_LENGTH; i++)
    {
      waveforms[ch][i] = 0;
    }
  }
}

// Constructors
void EventClass::resetEvent()
{
  for(Int_t i=0; i<userInp->HEADER_LENGTH; i++)
  {
    header[i] = 0;
  }
  for(Int_t ch=0; ch<userInp->NUM_CHANNELS; ch++)
  {
    for(Int_t i=0; i<userInp->RECORD_LENGTH; i++)
    {
      waveforms[ch][i] = 0;
    }
  }
  evtNum++;
}

void EventClass::deleteEvent()
{
  delete [] header;
  for(Int_t ch=0; ch<userInp->NUM_CHANNELS; ch++)
  {
    delete[] waveforms[ch];
  }

  delete [] waveforms;
}
