//
// LZTree.cpp
// Member functions for an output root tree
// Class for exporting "LZ-format" ROOT tree from pre-processed ROOT data
// Created by Nathan Giha (giha@umich.edu) on Mar. 13, 2023
// SLAC National Accelerator Laboratory / U. Michigan
//

#include "LZTree.h"

using namespace std;



LZTree::LZTree(UserInput* inUserInput)
{
  lzFile = new TFile(inUserInput->LZ_FILE_NAME, "RECREATE");
  lzFile->cd();

  InitGlobal();
  InitSummary();
  InitEvent();
  InitData();
  InitSumData();
}


void LZTree::InitGlobal()
{
  cout << "  Creating Global TTree" << endl;
  lzFile->cd();
  globalTree = new TTree("Global", "Global");
  globalTree->SetFileNumber(0);
  // globalTree->SetMaxTreeSize(10000000000LL);

  globalTree->Branch("runNumber", &triggerRunNumber, "runNumber/i");
  globalTree->Branch("runType", &runType, "runType/s");
  globalTree->Branch("firstEvent", &firstEv, "firstEvent/s");
  globalTree->Branch("numOfDigi", &numberOfDigitizers, "numberOfDigitizers[14]/b");
  globalTree->Branch("DCnCh", numberOfChannels, "numberOfChannels[14]/s");
  globalTree->Branch ("chDC0", &chDC0vec);
}

void LZTree::InitSummary()
{
  cout << "  Creating Summary TTree" << endl;
  lzFile->cd();
  summaryTree = new TTree("Summary", "Summary");
  summaryTree->SetFileNumber(0);
  // summaryTree->SetMaxTreeSize(10000000000LL);
  summaryTree->Branch("endFlag", &endFlag, "endFlag/s");
  summaryTree->Branch("nEvtsFile", &nEvtsFile, "nEvtsFile/s");
}

void LZTree::InitEvent()
{
  cout << "  Creating Event TTree" << endl;
  lzFile->cd();
  eventTree = new TTree("Event", "Event");
  eventTree->SetFileNumber(0);
  // eventTree->SetMaxTreeSize(10000000000LL);

  eventTree->Branch("globalEvt", &eventSeqNumber, "globalEvt/i");
  eventTree->Branch("firstData" ,&firstEvtData ,"firstData/i");
  eventTree->Branch("nPods", numOfPODs, "nPods[15]/s");
  eventTree->Branch("bufferStart", bufferLiveStartTS, "bufferStart[46]/l");
  eventTree->Branch("bufferStop", bufferLiveStopTS, "bufferStop[46]/l");
  eventTree->Branch("trgType", &triggerType_Short, "trgType/s");
  eventTree->Branch("trgTimeStamp", &triggerTimeStamp, "trgTimeStamp/l");
  eventTree->Branch("trgMultiplicity", &triggerMultiplicity, "trgMultiplicity/s");
}

void LZTree::InitData()
{
  cout << "  Creating Data TTree" << endl;
  lzFile->cd();
  dataTree = new TTree("Data", "Data");
  dataTree->SetFileNumber(0);
  // dataTree->SetMaxTreeSize(10000000000LL);

  dataTree->Branch("evt", &nEvtsFile, "evt/s");
  dataTree->Branch("channel", &channelID, "channel/s");
  dataTree->Branch("hit", &hitId, "hit/s");
  dataTree->Branch("startTime", &startTS, "startTime/l");
  dataTree->Branch("nSamples", &lengthPOD, "nSamples/s");
  dataTree->Branch("truncated", &truncPOD, "truncPOD/b");
  dataTree->Branch("zData", &myPODvec);
}

void LZTree::InitSumData()
{
  cout << "  Creating SumData TTree" << endl;
  lzFile->cd();
  sumDataTree = new TTree("SumData", "SumData");
  sumDataTree->SetFileNumber(0);
  // sumDataTree->SetMaxTreeSize(10000000000LL);

  sumDataTree->Branch("evt", &TRevt, "TRevt/s");
  sumDataTree->Branch("startTime", &TRstartTS, "TRstartTS/l");
  sumDataTree->Branch("nSamples", &TRlengthPOD, "TRlengthPOD/s");
  sumDataTree->Branch("truncated", &TRtruncPOD, "TRtruncPOD/b");
  sumDataTree->Branch("trData", &sumPODvec);
}



// void LZTree::writeToTree()
// {
//   // outFile = headerTree->GetCurrentFile();
//   globalTree->Write();
// }
void LZTree::closeFiles()
{
  // inData->outFile->Close();
  lzFile->Close();
}




// LZTree::loopData(TTree *tree) : fChain(0)
// {
// // if parameter tree is not specified (or zero), connect the file
// // used to generate this class and read the Tree.
//    if (tree == 0) {
//       TFile *f = (TFile*)gROOT->GetListOfFiles()->FindObject("dataFile.root");
//       if (!f || !f->IsOpen()) {
//          f = new TFile("dataFile.root");
//       }
//       f->GetObject("data",tree);
//
//    }
//    Init(tree);
// }

// void LZTree::killInData()
// {
//    if (!fChain) return;
//    delete fChain->GetCurrentFile();
// }
//
// Int_t LZTree::GetEntry(Long64_t entry)
// {
// // Read contents of entry.
//    if (!fChain) return 0;
//    return fChain->GetEntry(entry);
// }
// Long64_t LZTree::LoadTree(Long64_t entry)
// {
// // Set the environment to read one entry
//    if (!fChain) return -5;
//    Long64_t centry = fChain->LoadTree(entry);
//    if (centry < 0) return centry;
//    if (fChain->GetTreeNumber() != fCurrent) {
//       fCurrent = fChain->GetTreeNumber();
//       Notify();
//    }
//    return centry;
// }
//
// void LZTree::InitInHeaderTree(TTree* inHeaderTree)
// {
//    // Set branch addresses and branch pointers
//    if (!inHeaderTree) return;
//    fChain = inHeaderTree;
//    fCurrent = -1;
//    fChain->SetMakeClass(1);
//
//    fChain->SetBranchAddress("packetCounter", &packetCounter, &b_packetCounter);
//    fChain->SetBranchAddress("trigCounts", &trigCounts, &b_trigCounts);
//    fChain->SetBranchAddress("sumOut", &sumOut, &b_sumOut);
//    fChain->SetBranchAddress("timeCoarse", &timeCoarse, &b_timeCoarse);
//    fChain->SetBranchAddress("timeFine", &timeFine, &b_timeFine);
//    Notify();
// }
//
// void LZTree::InitInWfTree(TTree* inWfTree)
// {
//    // Set branch addresses and branch pointers
//    if (!inWfTree) return;
//    fChain = inWfTree;
//    fCurrent = -1;
//    fChain->SetMakeClass(1);
//
//    fChain->SetBranchAddress("evtNum", &evtNum, &b_evtNum);
//    fChain->SetBranchAddress("ch", &ch, &b_ch);
//    fChain->SetBranchAddress("waveform", waveform, &b_waveform);
//    Notify();
// }
//
// Bool_t LZTree::Notify()
// {
//    return kTRUE;
// }
//
// void LZTree::Show(Long64_t entry)
// {
// // Print contents of entry.
// // If entry is not specified, print current entry
//    if (!fChain) return;
//    fChain->Show(entry);
// }
// Int_t LZTree::Cut(Long64_t entry)
// {
// // This function may be called from Loop.
// // returns  1 if entry is accepted.
// // returns -1 otherwise.
//    return 1;
// }
