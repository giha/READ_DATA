//
// DataTree.cpp
// Member functions for an output root tree
// Class for exporting Root Tree from DT5560SE
// Created by Nathan Giha (giha@umich.edu) on Jan. 30, 2023
// SLAC National Accelerator Laboratory / U. Michigan
//

#include "DataTree.h"

using namespace std;

DataTree::DataTree(UserInput* inUserInput,
                   EventClass* event)
{
  input = inUserInput;
  outFile = new TFile(input->OUTPUT_FILE_NAME, "RECREATE");
  outFile->cd();

  headerTree = new TTree("headers", "headers");
  headerTree->SetFileNumber(0);
  headerTree->SetMaxTreeSize(1000000000LL);

  // Create branches for headers based on user input of header file
  for (Int_t i=0; i<input->HEADER_LENGTH; i++)
  {
    string headerName = (string)input->HEADER_STRUCTURE[i];

    if (headerName != "")
    {
      headerTree->Branch(headerName.c_str(),
                      &event->header[i],
                      (headerName + "/i").c_str());
    }
  }

  wfTree = new TTree("waveforms", "waveforms");
  wfTree->SetFileNumber(0);
  wfTree->Branch("evtNum", &event->evtNum, "evtNum/i");
  wfTree->Branch("ch", &chan, "ch/s");

  wf = new UShort_t[input->RECORD_LENGTH];

  wfTree->Branch("waveform", wf, ("waveform[" + to_string(input->RECORD_LENGTH) + "]/s").c_str());


  //
  // // Waveforms branch, stored as 1d array
  // char buf[100];
  // sprintf(buf,
  //         "waveforms[%i]/s",
  //         inUserInput->RECORD_LENGTH*inUserInput->NUM_CHANNELS);
  // headerTree->Branch("waveforms", event->waveforms, buf);


  // // Waveform branches
  // for (Int_t ch=0; ch<input->NUM_CHANNELS; ch++)
  // {
  //   string wfName = "wf_" + to_string(ch);
  //   headerTree->Branch(wfName.c_str(),
  //                   event->waveforms[ch],
  //                   (wfName + "[" + to_string(input->RECORD_LENGTH) + "]/s").c_str());
  // }
}

void DataTree::fillTrees(EventClass* event)
{
  headerTree->Fill();

  for (int ch=0; ch < input->NUM_CHANNELS; ch++)
  {
    // wf = event->waveforms[ch];

    for (int i=0; i < input->RECORD_LENGTH; i++)
    {
      wf[i] = event->waveforms[ch][i];
    }

    chan = ch;
    wfTree->Fill();
  }
}


void DataTree::writeToFile()
{
  outFile = headerTree->GetCurrentFile();
  headerTree->Write();
  wfTree->Write();
}
void DataTree::closeFile()
{
  outFile->Close();
}
