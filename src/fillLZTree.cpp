//
// fillLZTree.cpp
// Member functions for filling output root tree
// Function for filling "LZ-format" ROOT tree from pre-processed ROOT data
// Created by Nathan Giha (giha@umich.edu) on Mar. 30, 2023
// SLAC National Accelerator Laboratory / U. Michigan
//


#include "LZTree.h"

using namespace std;

void LZTree::FillLZTree(UserInput* inUserInput,
                   DataTree* inData)
{

  headers = new readHeaders(inData->headerTree);
  wfs     = new readWfs(inData->wfTree);

  if (headers->fChain == 0) return;

  // Set global stuff
  triggerRunNumber = 1;
  runType = 1;
  firstEv = 1;

  for (Int_t i=0; i < 14; i++)
  {
    numberOfDigitizers[i] = 0;
    numberOfChannels[i] = 0;
  }
  // Manually set just the first data collector
  numberOfDigitizers[0] = 1;
  numberOfChannels[0] = inUserInput->NUM_CHANNELS;

  for (Int_t ch=0; ch < numberOfChannels[0]; ch++)
  {
    chDC0vec[ch] = ch;
  }

  globalTree->Fill();

  // End set global stuff



  Long64_t nentries = headers->fChain->GetEntriesFast();
  // Warning: there is no protection for the "headers" and "wfs" trees
  // having mismatched sizes. It will loop over the "wfs" tree as
  // (# of header entries) * (# of channels)
  // - N. Giha 4/5/23

  Long64_t nbytes = 0, nb = 0;
  Long64_t wfnbytes = 0, wfnb = 0;

  // Loop over events
  for (Long64_t jentry=0; jentry < nentries; jentry++)
  {
    Long64_t ientry = headers->LoadTree(jentry);
    if (ientry < 0) break;
    nb = headers->fChain->GetEntry(jentry);   nbytes += nb;
    // if (Cut(ientry) < 0) continue;

    // Set event-level stuff
    eventSeqNumber = headers->packetCounter; //(UInt_t)jentry;
    firstEvtData = inUserInput->NUM_CHANNELS;
    for (Int_t i=0; i < 15; i++) {numOfPODs[i] = 0;}
    numOfPODs[0] = inUserInput->NUM_CHANNELS;
    bufferLiveStartTS[0] = -1;
    bufferLiveStopTS[0] = -1;
    triggerType_Short = 1;
    triggerTimeStamp = headers->timeCoarse;
    triggerMultiplicity = headers->sumOut;

    // cout << "Filling event tree" << endl;
    eventTree->Fill();

    // Loop over waveforms in event
    for (Int_t ch=0; ch < inUserInput->NUM_CHANNELS; ch++)
    {
      Long64_t wentry = wfs->LoadTree(jentry*inUserInput->NUM_CHANNELS+ch);
      if (wentry < 0) break;
      wfnb = wfs->fChain->GetEntry(wentry);   wfnbytes += wfnb;

      nEvtsFile = jentry;//wfs->evtNum;
      channelID = wfs->ch;
      hitId     = wfs->ch;
      startTS = 0;
      lengthPOD = inUserInput->RECORD_LENGTH;
      truncPOD = 0;
      myPODvec.resize(lengthPOD);
      // myPODvec = wfs->waveform;
      myPODvec.assign(wfs->waveform, wfs->waveform + inUserInput->RECORD_LENGTH);

      // cout << "Filling data tree" << endl;
      dataTree->Fill();
    }

    if (jentry % 100 == 0) cout << " Filled entry " << jentry << endl;
  }

  /*
  // Get DataTree
  cout << "Getting data tree" << endl;
  inHeaderTree = inData->headerTree;
  InitInHeaderTree(inHeaderTree);

  // Set "once per file" stuff
  cout << "Getting entries: " << inHeaderTree->GetEntries() << endl;
  triggerRunNumber = 1;
  numberOfDigitizers[0] = 1;
  numberOfChannels[0] = inUserInput->NUM_CHANNELS;
  nEvtsFile = (UShort_t)inHeaderTree->GetEntries();

  cout << "Writing LZ file trees" << endl;
  lzFile->cd();

  if (fChain == 0) return;

  Long64_t nentries = fChain->GetEntriesFast();

  Long64_t nbytes = 0, nb = 0;
  for (Long64_t jentry=0; jentry<nentries;jentry++)
  {
     Long64_t ientry = LoadTree(jentry);
     if (ientry < 0) break;
     nb = fChain->GetEntry(jentry);   nbytes += nb;
     // if (Cut(ientry) < 0) continue;

     // Set event-level stuff
     eventSeqNumber = (UInt_t)jentry;
     bufferLiveStartTS[0] = -1;
     bufferLiveStopTS[0] = -1;
     triggerType_Short = 1;
     triggerMultiplicity = sumOut;

     // cout << "Filling event tree" << endl;
     eventTree->Fill();



     // Loop over waveforms
     for (int ch=0; ch < inUserInput->NUM_CHANNELS; ch++)
     {
       nEvtsFile = eventSeqNumber;
       channelID = ch;
       startTS = 0;
       lengthPOD = inUserInput->RECORD_LENGTH;
       // myPODvec.resize(lengthPOD);

       // Set waveform stuff
       // for (int i=0; i<lengthPOD; i++)
       // {
       //   myPODvec[i] =
       // }

       // cout << "Filling data tree" << endl;
       dataTree->Fill();
     }

  }
  */

  cout << "Filling summary tree" << endl;
  endFlag = 0;
  nEvtsFile++; // Increment by one to give event count instead of index
  summaryTree->Fill();

  cout << "Filled total " << nEvtsFile << " entries" << endl;
  cout << "Writing to LZap-formatted ROOT file " << inUserInput->LZ_FILE_NAME << endl;
  lzFile->Write();


  // killInData();
}
