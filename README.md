# READ_DATA
 Repo for reading raw data from DT5560SE into ROOT tree

 # Directions for compiling and running
 1. Create build directory
 2. cd into build directory
 3. cmake <this directory>
 4. make
 5. Execute with input file

 # The <RAW_FILE_NAME> in your input file should point to the file
 #   you intend to convert to ROOT format


 # Example cmdline input, starting in READ_DATA
 1. mkdir ../buildRaw
 2. cd ../buildRaw
 3. cmake ../READ_DATA
 4. make
 5. ./READ_DATA ../READ_DATA/read.in
